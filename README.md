# cis2250_20192020_research_emerging_google_samples

This is the repository where Kylie Dolson's research topic components are kept. 

The research topic I am doing is Google Samples, and more specifically, AppUsageStatistics.

In this repository you will find the following: 

1. A document to outline my research. 
2. A powerpoint (google slides) for my research presentation. 
3. An example involving AppUsageStatistics.